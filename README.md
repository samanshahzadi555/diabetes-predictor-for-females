# Diabetes Predictor for Females 

This web page is built using Html,CSS,Python and Flask.

Diabetes predictor predicts  the occurrence of diabetes in a female on the basis of some factors.

It uses the following explanatory variables for calculation: 

1.  Number of times pregnant 
1.  Plasma glucose concentration 
1.  Diastolic blood pressure 
1.  Skin thickness 
1.  2-Hour serum insulin
1.  BMI 
1.  Diabetes pedigree function 
1.  Age

**Configuration Steps**

1. **Front end files :** Index.html , result.html

1. Index.html contains form where user will input values.

1. result.html contains the result of prediction and graph plots.

1. **Backend files :** Project.py , Predictor.py

1. Project.py file connects the app with front end files.

1. Predictor.py contains the following information:


    - List of libraries Imported
    - Reading Dataset values
    - Pre-processing on dataset values i.e. removing zero or NULL values from dataset, renaming of columns
    - Training : On 70% dataset values
    - Testing :  On 30% dataset values
    -  Finding effect of each feature by measuring accuracy values one by one 
    -  Plotting graphs : Histograms --> Data Distribution , Graph --> Comparison



_**ScreenShots**_


**Home page**

<img src="static/1.JPG">

**Person does not have diabetes**

<img src="static/13.JPG">
<img src="static/14.JPG">

**Person has diabetes**

<img src="static/2.JPG">
<img src="static/3.JPG">

**Data Distribution histograms for each test**

<img src="static/4.JPG">
<img src="static/5.JPG">
<img src="static/6.JPG">
<img src="static/7.JPG">

**Data Distribution of all quantities in one plot**

<img src="static/8.JPG">

**Effect of each feature on Diabetes prediction (Accuracy Values)**

<img src="static/9.JPG">

**Comparison of dependency of diabetes on different factors**

<img src="static/10.JPG">

**Most Responsible factor**

<img src="static/11.JPG">

Diabetes Predicted
<video src="static/Project_Demo_Video1.mp4" type="video/mp4">


Diabetes not Predicted
<video src="static/Project_Demo_Video2.mp4" type="video/mp4">
